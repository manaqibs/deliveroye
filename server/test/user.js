//During the test the env variable is set to test
// process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let UserModel = require('../models/user.model');
const url = require('../mongo').mongoUri;
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../index');
let should = chai.should();

chai.use(chaiHttp);

const userData = { name: 'TekLoon',firstname: 'aBhatti', sex: 'male', username: 'al1i22neww1', password:'123', phonenumber:'1234567', useremail: 'ali@demo.com' };

describe('User Model Test', () => {

    // It's just so easy to connect to the MongoDB Memory Server 
    // By using mongoose.connect
    beforeEach(async () => {
        await mongoose.connect(url, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true }, (err) => {
            if (err) {
                console.error(err);
                process.exit(1);
            }
        });
    });

    it('it should GET all the users', (done) => {
            chai.request(server)
            .get('/api/user')
            .end((err, res) => {
                res.should.have.status(200);
            done();
            });
    });

    it('it should not POST a user without pages field', (done) => {
        let user = userData
          chai.request(server)
          .post('/api/user')
          .send(user)
          .end((err, res) => {
                // console.log(err);
                res.should.have.status(200);
                res.body.should.be.a('object');
            done();
          });
    });

    it('it should not PUT a user without pages field', (done) => {
        let user = userData
        chai.request(server)
            .put('/api/user?id=2')
            .send(user)
            .end((err, res) => {
                // console.log(err);
                res.should.have.status(200);
                res.body.should.be.a('object');
            done();
            });
    });

    it('it should delete user with given id', (done) => {
        chai.request(server)
            .delete('/api/user?id=2')
            .end((err, res) => {
                // console.log(err);
                res.should.have.status(200);
                res.body.should.be.a('object');
            done();
            });
    });
});
