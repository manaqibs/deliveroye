let mongoose = require("mongoose");
let PackageModel = require('../models/package.model');
const url = require('../mongo').mongoUri;
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../index');
let should = chai.should();

chai.use(chaiHttp);

const packageData = {weight: 2, height: 15, length: 10, widht: 10, description: 'This is test', dateofdelivery: Date(2020,1,1), placeofdelivery: 'Magdeburg', placeofpickup: 'Berlin'};

describe('Package Model Test', () => {

    // It's just so easy to connect to the MongoDB Memory Server 
    // By using mongoose.connect
    beforeEach(async () => {
        await mongoose.connect(url, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true }, (err) => {
            if (err) {
                console.error(err);
                process.exit(1);
            }
        });
    });

    it('it should GET the package by id', (done) => {
            chai.request(server)
            .get('/api/package?id=1')
            .end((err, res) => {
                res.should.have.status(200);
            done();
            });
    });

    it('it should GET all the package by filter criteria', (done) => {
        chai.request(server)
        .get('/api/packages')
        .end((err, res) => {
            res.should.have.status(200);
        done();
        });
    });

    it('it should not POST a package without pages field', (done) => {
        let package = packageData
            chai.request(server)
            .post('/api/package')
            .send(package)
            .end((err, res) => {
                // console.log(err);
                res.should.have.status(200);
                res.body.should.be.a('object');
            done();
            });
    });

    it('it should not PUT a package without pages field', (done) => {
        let package = packageData
        chai.request(server)
            .put('/api/package?id=1')
            .send(package)
            .end((err, res) => {
                // console.log(err);
                res.should.have.status(200);
                res.body.should.be.a('object');
            done();
            });
    });

    it('it should delete package with given id', (done) => {
        chai.request(server)
            .delete('/api/package?id=1')
            .end((err, res) => {
                // console.log(err);
                res.should.have.status(200);
                res.body.should.be.a('object');
            done();
            });
    });
});