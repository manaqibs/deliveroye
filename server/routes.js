const express = require("express");
const router = express.Router();

const userService = require("./services/user.service");
const packageService = require("./services/package.service");

router.get("/user", (req, res) => userService.getUser(req, res));
router.get("/user", (req, res) => userService.getUsers(req, res));
router.get("/lastuser", (req, res) => userService.getLastUser(req, res));
router.post("/user", (req, res) => userService.postUsers(req, res));
router.put("/user", (req, res) => userService.putUsers(req, res));
router.delete("/user", (req, res) => userService.deleteUsers(req, res));

router.get("/package", (req, res) => packageService.getPackage(req, res));
router.get("/packages", (req, res) => packageService.getPackages(req, res));
router.get("/lastpackage", (req, res) =>
  packageService.getLastPackage(req, res)
);
router.post("/package", (req, res) => packageService.postPackage(req, res));
router.put("/package", (req, res) => packageService.putPackage(req, res));
router.delete("/package", (req, res) => packageService.deletePackage(req, res));

module.exports = router;
