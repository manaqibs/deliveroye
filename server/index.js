// require dependencies
// ========================================================================================
const express = require('express');
var cors = require('cors');
const bodyParser = require('body-parser');
const path = require('path');
const routes = require('./routes');
const pino = require('express-pino-logger')();
const CircularJSON = require('circular-json')
let MongoClient = require('mongodb').MongoClient;

// initialize variables
// ========================================================================================
const root = './';
const app = express();
app.use(cors());

// define middleware
// ========================================================================================
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use(pino);
// app.use(express.static((path.join(root,'dist'))));
app.use('/api',routes);

app.get('*',(req,res)=>{
    res.json('Something went wrong!!');
})

// start server
// ========================================================================================
const port = process.env.PORT || 3000;
var server = app.listen(port, () =>
  console.log('Express server is running on localhost: ',port)
);

module.exports = server;