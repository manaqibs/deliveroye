const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    id: {type: Number, auto: true},
    name: { type: String, required: true },
    firstname: { type: String, required: true },
    sex: { type: String, enum: ['male', 'female'], required: true },
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    phonenumber: {type: String},
    useremail: { type: String, required: true },
},{
    timestamps:true
});
const User = mongoose.model('User', userSchema);
module.exports = User;