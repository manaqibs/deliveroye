const mongoose = require('mongoose');

const packageSchema = new mongoose.Schema({
    id: {type: Number, auto: true},
    weight: { type: Number, required: true },
    height: { type: Number, required: true },
    length: { type: Number, required: true },
    widht: { type: Number, required: true },
    description: {type: String, required: true},
    dateofdelivery: { type: Date},
    placeofdelivery: {type: String},
    placeofpickup:{type: String}
},{
    timestamps:true
});
const Package = mongoose.model('Package', packageSchema);
module.exports = Package;
