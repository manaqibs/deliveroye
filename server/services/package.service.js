let MongoClient = require('mongodb').MongoClient;
const Package = require('../models/package.model');

const url = require('../mongo').mongoUri;

function getPackage(req, res) {
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("deliveroye");
        dbo.collection("package").findOne({
          id: req.query.id
        }, 
        function(err, result) {
            if (err) throw err;
            res.setHeader('Content-Type', 'application/json');
            res.send(result);
            db.close();
        });
    });
}

function getPackages(req, res) {
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("deliveroye");
        dbo.collection("package").find({}).toArray(
          function(err, result) {
            if (err) throw err;
            res.setHeader('Content-Type', 'application/json');
            res.send(result);
            db.close();
        }
        );
    });
}

function postPackage(req, res) {
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        const package = new Package(req.body);
        var dbo = db.db("deliveroye");
        dbo.collection("package").insertOne(package,
            function(err, result) {
                if (err) throw err;
                res.setHeader('Content-Type', 'application/json');
                res.send(result);
                db.close();
            });
    });
}

function putPackage(req, res) {
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        const package = new Package(req.body);
        const query = {id: req.query.id};
        var dbo = db.db("deliveroye");
        dbo.collection("package").updateOne(query, package,
            function(err, result) {
                if (err) throw err;
                res.setHeader('Content-Type', 'application/json');
                res.send(result);
                db.close();
            });
    });
}

function deletePackage(req, res) {
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        const package = new Package(req.body);
        const query = {id: req.query.id};
        var dbo = db.db("deliveroye");
        dbo.collection("package").deleteOne(query,
            function(err, result) {
                if (err) throw err;
                res.setHeader('Content-Type', 'application/json');
                res.send(result);
                db.close();
            });
    });
}
const pipelineLast = [{
      '$sort': {
        'id': -1
      }
    }, {
      '$limit': 1
    }];

function getLastPackage(req, res) {
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("deliveroye");
        dbo.collection("package").aggregate(pipelineLast).toArray(
            function(err, result) {
              if (err) throw err;
              res.setHeader('Content-Type', 'application/json');
              res.send(result);
              db.close();
          });
    });
}


module.exports = {getPackage, getPackages, postPackage, putPackage, deletePackage, getLastPackage};