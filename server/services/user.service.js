let MongoClient = require("mongodb").MongoClient;
const User = require("../models/user.model");

const url = require("../mongo").mongoUri;

function getUser(req, res) {
  MongoClient.connect(url, function (err, db) {
    if (err) throw err;
    var dbo = db.db("deliveroye");
    dbo.collection("user").findOne(
      {
        useremail: req.query.useremail,
        password: req.query.password,
      },
      function (err, result) {
        if (err) throw err;
        res.setHeader("Content-Type", "application/json");
        res.send(result);
        db.close();
      }
    );
  });
}

function getUsers(req, res) {
  MongoClient.connect(url, function (err, db) {
    if (err) throw err;
    var dbo = db.db("deliveroye");
    dbo
      .collection("user")
      .find({})
      .toArray(function (err, result) {
        if (err) throw err;
        res.setHeader("Content-Type", "application/json");
        res.send(result);
        db.close();
      });
  });
}

function postUsers(req, res) {
  MongoClient.connect(url, function (err, db) {
    if (err) throw err;
    const user = new User(req.body);
    console.log("postUsers, Rquest data.body: ", req.body, user, req);
    var dbo = db.db("deliveroye");
    dbo.collection("user").insertOne(user, function (err, result) {
      if (err) throw err;
      // res.setHeader("Content-Type", "application/json");
      // res.send(result);
      dbo.collection("user").findOne(
        {
          useremail: req.body.useremail,
          password: req.body.password,
        },
        function (err, result) {
          if (err) throw err;
          res.setHeader("Content-Type", "application/json");
          res.send(result);
          db.close();
        }
      );
      db.close();
    });
  });
}

function putUsers(req, res) {
  MongoClient.connect(url, function (err, db) {
    if (err) throw err;
    const user = new User(req.body);
    const query = { id: req.query.id };
    var dbo = db.db("deliveroye");
    dbo.collection("user").updateOne(query, user, function (err, result) {
      if (err) throw err;
      res.setHeader("Content-Type", "application/json");
      res.send(result);
      db.close();
    });
  });
}

function deleteUsers(req, res) {
  MongoClient.connect(url, function (err, db) {
    if (err) throw err;
    const user = new User(req.body);
    const query = { id: req.query.id };
    var dbo = db.db("deliveroye");
    dbo.collection("user").deleteOne(query, function (err, result) {
      if (err) throw err;
      res.setHeader("Content-Type", "application/json");
      res.send(result);
      db.close();
    });
  });
}

const pipelineLast = [
  {
    $sort: {
      id: -1,
    },
  },
  {
    $limit: 1,
  },
];

function getLastUser(req, res) {
  MongoClient.connect(url, function (err, db) {
    if (err) throw err;
    var dbo = db.db("deliveroye");
    dbo
      .collection("user")
      .aggregate(pipelineLast)
      .toArray(function (err, result) {
        if (err) throw err;
        res.setHeader("Content-Type", "application/json");
        res.send(result);
        db.close();
      });
  });
}

module.exports = {
  getUser,
  getUsers,
  postUsers,
  putUsers,
  deleteUsers,
  getLastUser,
};
