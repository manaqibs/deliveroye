import React from "react";
import {
  Login,
  Home,
  Request,
  NotFound
 } from "../Views";

const routesObj = {
  Root: {
    path: "/",
    component: <Login />,
    roles: [],
    fallback: null,
  },
  Login: {
    path: "/login",
    component: <Login />,
    roles: [],
    fallback: null,
  },
  Home: {
    path: "/home",
    component: <Home />,
    roles: [],
    fallback: null,
  },
  Request: {
    path: "/request",
    component: <Request />,
    roles: [],
    fallback: null,
  },

  NotFound: {
    path: "*",
    component: <NotFound />,
    roles: [],
    fallback: null,
  },
};
export default routesObj;
