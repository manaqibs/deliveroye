import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import {
  Login,
  Home,
  Request,
  FindTravelor,
  TravellerList,
  MyDeliveries,
  MyTravels,
  DeliveryDetail,
  Account,
  NotFound
 } from "../Views";


class Routes extends Component {
  render() {
    return (
      <Router>

        <Switch>
        <Route
            exact path="/"
            component={Home}
            />
          <Route
            exact path="/login"
            component={Login}
            />
           <Route
            exact path="/home"
            component={Home}
            />
            <Route
            exact path="/request"
            component={Request}
            />
            <Route
            exact path="/find-traveler"
            component={FindTravelor}
            />
            <Route
            exact path="/traveller-list"
            component={TravellerList}
            />
             <Route
            exact path="/my-deliveries"
            component={MyDeliveries}
            />
             <Route
            exact path="/my-travels"
            component={MyTravels}
            />
            <Route
            exact path="/delivery-detail/:id"
            component={DeliveryDetail}
            />
            <Route
            exact path="/account"
            component={Account}
            />

          <Route
            exact
            path="*"
            component={NotFound}
          />

        </Switch>
      </Router>
    );
  }
}

export default Routes;
