import axios from "axios";
import { apiURL, UsersAPIs } from "../api/config";

export const getUser = (query) =>
  axios.get(UsersAPIs.GetUser, {
    params: query,
  });
export const RegisterUser = async (user) =>
  axios.post(UsersAPIs.RegisterUser, user);
