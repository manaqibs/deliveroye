import React, {useState, useEffect} from 'react';
import {Header, AppBtn, FormFields, Modal, ModalLogin} from '.././../Components/Common/index';
import { useHistory } from "react-router-dom";
import {homescreen, driver, smallImage, sender} from '../../resources/images/index';
import {bindActionCreators} from 'redux';
import {usernameActions} from '../../redux/actions/userActions.js';
import {connect} from 'react-redux';
import Cards from './Cards';
import HowItWorks from './HowItWorks';
import AboutUs from './AboutUs';
import ContactUs from './ContactUs';


const Home = (props) => {
    const [showLogin, setShowLogin] = useState(false);
    const [showTravele, setShowTravele] = useState(false)

    const history = useHistory();
    const travelist = () => history.push('/traveller-list');

    const onFormSubmit = (e) => {
        e.preventDefault();
    }

useEffect(() => {
   props.action()
    },[]
);


const readMore = () => {
    history.push('/request');
}

const cardData = [
    {
        img:driver,
        title:"Driver",
        heading:"Get there faster with a more comfortable ride",
        text:"User our trip planner and find carpool rides together with punlic transit results",
        buttonText:"read more",
        handleText:readMore,
    },
    {
        img:sender,
        title:"Sender",
        heading:"Get there faster with a more comfortable ride",
        text:"User our trip planner and find carpool rides together with punlic transit results",
        buttonText:"read more",
        handleText:readMore,
    },
]


    return (
        <div className="homeWrapper">
            <Header />
            <div className="picture" style={{backgroundImage: `url(${homescreen})`}}>
                <div className="homeInner">
                <AppBtn text="Find a traveler" onClick={() => setShowTravele(true)}/>
                <AppBtn text="Add a traveler" onClick={() => setShowLogin(true)}/>
                </div>
            </div>
            <div className="infoCards" id="infocard">

               {cardData.map((items, index) => {
                   return <Cards
                   key={index}
                   img={items.img}
                   title={items.title}
                   heading={items.heading}
                   text={items.text}
                   buttonText={items.buttonText}
                   handleText={items.handleText}
                   style={{backgroundImage: `url(${smallImage})`}}
               />
               })}
            </div>

            <HowItWorks />
            <AboutUs />
            <ContactUs />

            {/* login screen modal */}
            {showLogin &&
                <ModalLogin
                    handleCLose = {() => setShowLogin(false)}
                    props
                    handleSubmit = {travelist}
                    style={{backgroundImage: `url(${smallImage})`}}
                />
            }

             {/* traveler screen modal    */}
           {showTravele &&
            <Modal
            closeModal = {() => setShowTravele(false)}
            submitModal = {() => setShowTravele(false)}
            buttontext = "Search"
            title = "Find traveler"
            >
            <form onSubmit={onFormSubmit}>
             <FormFields  type="text" placeholder="From" />
             <FormFields  type="text" placeholder="To" />
             <FormFields  type="date" placeholder="Date" />
             <FormFields  type="number" min="0" placeholder="Weight" />
            </form>
          </Modal>
           }

        </div>
    );
};

const mapStatetoProps = (state) => {
    return state
}

const mapDispatchtoProps = (dispatch) => ({
     action:  bindActionCreators (usernameActions, dispatch)
})

export default connect(mapStatetoProps, mapDispatchtoProps)(Home);