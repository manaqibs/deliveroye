import React from 'react';
import {howworks} from '../../resources/images/index'

function HowItWorks(props) {
    return (
        <div className="howItWorks"  id="howitwork">
           <h1>How it works</h1> 
           <p>DeliverOye is a mobile, cloud-based solution for companies, communities and events which supports the organization of carpools of employees, residents and event participants. </p>
            <div className="howWorksBody">
                <div className="howWorksLeft">
                    <div className="howWorksSteps">
                        <span className="stepNumer"> 1</span>
                        <span className="stepText">
                            <h1>Set up an account</h1>
                            You can download the DeliverOye App for free or access the website at www.DeliverOye.com. After registration, confirm your email address via an email that is sent to your in-box. Then you can get started!</span>
                    </div>
                    <div className="howWorksSteps">
                        <span className="stepNumer"> 2</span>
                        <span className="stepText">
                        <h1>Set a ride request</h1>
                        Now create your ride request on DeliverOye as a driver, passenger, or both. Enter your starting point and destination as well as departure time and arrival time. Afterwards, DeliverOye will get started with the search!</span>
                    </div>
                    <div className="howWorksSteps">
                        <span className="stepNumer"> 3</span>
                        <span className="stepText">
                        <h1>A carpool is found</h1>
                        DeliverOye independently compares similar routes and times right away. If DeliverOye finds a suitable carpool, the system brings the driver and passengers together fully automatically. Practical!</span>
                    </div>
                    <div className="howWorksSteps">
                        <span className="stepNumer"> 4</span>
                        <span className="stepText">
                        <h1>Accept the carpooling offer</h1>
                        Drivers and passengers will be informed automatically of the “match” by text message, email, or App up to five minutes before the desired start time. Get in – hit the road – save costs!</span>
                    </div>
                </div>
                <div className="howWorksRight">
                    <img src={howworks} alt="" />
                </div>
            </div>
        </div>
    );
}

export default HowItWorks;