import React, {useStste, useEffect, useState} from 'react';
import {Header} from '../../Components/Common/index';
import DeliveryCard from './DeliveryCard';
import {avatar} from '../../resources/images/index';
import {useHistory} from 'react-router-dom';
import { connect } from 'react-redux';
import * as userActions from '../../redux/actions/userActions'
import {bindActionCreators} from 'redux';
import moment from 'moment';

import {getPackages} from '../../services/packageServices';

const DeliveryList = (props) => {
    const [data, setData] = useState([])

      useEffect(() => {
       packagelist();
    },[])

    const history = useHistory();

    const deliverydetail = (itemid) => history.push(`/delivery-detail/${itemid}`)

    const packagelist = () => getPackages()
        .then((response) => {
        //   console.log(response)
          setData(response.data)
        })


    return (
        <div className="mydeliveriesWrapper">

            <div className="deliveriesInner">
                {data && data.map((item, index) => {
                  return  <DeliveryCard key={index}
                                id= {item.id}
                                img={item.img}
                                name={item.name}
                                requesttext="Detail"
                                tracktext="Track"
                                date={`${moment(item.dateofdelivery).format("Do MMMM YYYY")}`}
                                weight={item.weight}
                                charges={item.charges}
                                placeofdelivery={item.placeofdelivery}
                                placeofpickup={item.placeofpickup}
                                submitrequest={deliverydetail}
                                // imgURL={deliverydetail}
                                // nameURL={deliverydetail}
                                // submittrack={deliverydetail}
                            />
                         }) }
            </div>

        </div>
    );
};

const mapStatetoProps = (userdetail) => {
     const {userDetail} = userdetail;
     return {userDetail}
}

const mapDispatchtoProps = (dispatch) => {
   return {
       userActions: bindActionCreators(userActions, dispatch)
   }
}

export default connect(mapStatetoProps, mapDispatchtoProps  ) (DeliveryList);