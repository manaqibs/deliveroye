import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux'
import {Header} from '../../Components/Common/index'
import {avatar} from '../../resources/images/index'
import { FaChevronLeft, FaMapMarkerAlt, FaPrint } from "react-icons/fa";
import {GrPrint, GrCloudDownload} from 'react-icons/gr';
import {Link,  useParams} from 'react-router-dom';
import * as userActions from '../../redux/actions/userActions'
import {getPackages} from '../../services/packageServices';
import {bindActionCreators} from 'redux';
import moment from 'moment';
import axios from 'axios'



const DeliveryDetail = (props) => {
        const [data, setData] = useState([])

        let { id } = useParams();

        const packagedetail = () => getPackages()
        .then((response) => {
          const result = response.data.filter(item => item.id === id)
          setData(result[0])
        })

    useEffect(() => {
        packagedetail()
    },[])

     const defaultdate = data.dateofdelivery;


    return (
        <div className="mydeliveriesWrapper">
            <Header />
            <div className="detailsInner">
             <h1><i><Link to="/my-deliveries"><FaChevronLeft/></Link></i>Package details</h1>
             {data ?  <>
             <div className="contsctDetailWrapper">
                 <div className="addressDetail">
                     <span><i>Package number:</i> </span>
                     <span><i>Date:</i>{defaultdate && `${moment(defaultdate).format("Do MMMM YYYY")}`} </span>
                     <span><i>Charges:</i>$</span>
                     <span><i>Dropout address:</i> {data.placeofpickup && data.placeofpickup}</span>
                     <span><i>Delivery address:</i>{data.placeofdelivery && data.placeofdelivery}</span>
                     <span><i>Weight:</i>{data.weight && data.weight}</span>
                 </div>
                 <span className="avatar">
                    <i><img src={avatar} alt="" /></i>
                     <span>Muhammad Javad Iqbal</span>
                 </span>
             </div>


             <div className="cardmiddle">
             <span className="leftLocation">{data.placeofpickup && data.placeofpickup}</span>
             <div>
                 <span className="travelerDestination"><i>9:15</i> <i>17:00</i></span>
                <span className="travelerLine"></span>
                <span className="travelerDestination"><i><FaMapMarkerAlt />Ostbahnhof</i> <i><FaMapMarkerAlt />Laim</i></span>
             </div>
                <span className="rightLocation">{data.placeofdelivery && data.placeofdelivery}</span>
            </div>

            <div className="packageDescription">
                <h2>Package Description</h2>
                <p>{data.description && data.description}</p>
             </div>
             </> : <span>No data found</span> }



            <div className="packageFooter">
                <GrPrint />
                <GrCloudDownload />
            </div>
           </div>
      </div>
    );
}

const mapStatetoProps = (userdetail) => {
     const {userDetail} = userdetail;
     return {userDetail}
}

const mapDispatchtoProps = (dispatch) => {
   return {
       userActions: bindActionCreators(userActions, dispatch)
   }
}

export default connect(mapStatetoProps, mapDispatchtoProps)(DeliveryDetail);