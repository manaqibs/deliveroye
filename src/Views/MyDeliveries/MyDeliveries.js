import React from 'react';
import {Header} from '.././../Components/Common/index';
import DeliveryList from './DeliveryList';

function MyDeliveries(props) {
    return (
        <div className="mydeliveriesWrapper">
            <Header />           
             <DeliveryList />
        </div>
    );
}

export default MyDeliveries;