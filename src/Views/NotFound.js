import React from 'react';

function NotFound(props) {
  return (
    <div className="main-container notFoundWrapper">
      <span>&#9785;</span> Page not found.
    </div>
  );
}

export default NotFound;
