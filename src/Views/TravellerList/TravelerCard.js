import React from 'react';

const TravelerCard = ({img, name,  deliverspot, weight, city, amount, requesttext, submitrequest}) => {
    return (
        <div className="travelerCardWrapper">
            <div className="cardLeft">
                <span className="userImage"><img src={img} alt="" /></span>
                <span className="travelerName">{name}</span>
                <span className="travelerCity">{city}</span>
                <span className="travelerspace">Available space: <i>{weight}</i></span>
            </div>
            <div className="cardmiddle">               
                <span className="travelerDate"><i>12:00</i> <i>19:00</i></span>
                <span className="travelerLine"></span>
                <span className="travelerDestination"><i>Berlin</i> <i>Munich</i></span>
            </div>
            <div className="cardRight">
                <span className="travelerAmount">${amount}</span>
                <span className="travelerSpot">Delivery spot: {deliverspot}</span>
                <span className="travelerBtnWrapper">
                    <button onClick={submitrequest}>{requesttext}</button>
                </span>

            </div>
        </div>
    );
};

export default TravelerCard;