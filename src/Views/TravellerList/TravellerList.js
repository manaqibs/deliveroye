import React from 'react';
import {Header} from '../../Components/Common/index';
import TravelerCard from './TravelerCard';
import {avatar} from '../../resources/images/index';
import {useHistory} from 'react-router-dom';

const travelercard = [
    {
        img:avatar,
        name:"First card", 
        requesttext:"Request", 
        city:"Chemnitz - Munich" ,
        weight:"8kg",
        amount:"35", 
        deliverspot:"Munich HBF" 
    },
    {
        img:avatar,
        name:"First card", 
        requesttext:"Request", 
        city:"Chemnitz - Munich" ,
        weight:"8kg",
        amount:"35", 
        deliverspot:"Munich HBF" 
    },
    {
        img:avatar,
        name:"First card", 
        requesttext:"Request", 
        city:"Chemnitz - Munich" ,
        weight:"8kg",
        amount:"35", 
        deliverspot:"Munich HBF" 
    },
    {
        img:avatar,
        name:"First card", 
        requesttext:"Request", 
        city:"Chemnitz - Munich" ,
        weight:"8kg",
        amount:"35", 
        deliverspot:"Munich HBF" 
    },
]


const TravellerList = () => {

    const history = useHistory();
const request = () => history.push('/request')


    return (
        <div className="traveleListWrapper">
            <Header />
            <div className="traveleListBody">
                {travelercard.map((items, index) => {
                  return  <TravelerCard key={index}
                                img={items.img}
                                name={items.name} 
                                requesttext={items.requesttext} 
                                city={items.city} 
                                weight={items.weight} 
                                amount={items.amount} 
                                deliverspot={items.deliverspot}
                                submitrequest={request}
                            />
                         }) }
            </div>
           
        </div>
    );
};

export default TravellerList;