export { default as Login } from './Login/Login';
export { default as Home } from './Home/Home';
export { default as Request } from './Request/Request';
export { default as NotFound } from "./NotFound";
export {default as FindTravelor} from './FindTravelor/FindTravelor';
export {default as AddTraveller} from './AddTraveller/AddTraveller';
export {default as TravellerList} from './TravellerList/TravellerList';
export {default as MyTravels} from './MyTravels/MyTravels';
export {default as MyDeliveries} from './MyDeliveries/MyDeliveries';
export {default as DeliveryDetail} from './MyDeliveries/DeliveryDetail';
export {default as Account} from './Account/Account';