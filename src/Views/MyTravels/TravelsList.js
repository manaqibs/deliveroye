import React from 'react';
import {Header} from '../../Components/Common/index';
import TravelsCard from './TravelsCard';
import {avatar} from '../../resources/images/index';
import {useHistory} from 'react-router-dom';

const travelercard = [
    {
        img:avatar,
        name:"First card", 
        requesttext:"Request", 
        date:"3 March 2021" ,
        weight:"8kg",
        amount:"35", 
        deliverspot:"Munich HBF" 
    },
    {
        img:avatar,
        name:"First card", 
        requesttext:"Request", 
        date:"3 March 2021" ,
        weight:"8kg",
        amount:"35", 
        deliverspot:"Munich HBF" 
    },
    {
        img:avatar,
        name:"First card", 
        requesttext:"Request", 
        date:"3 March 2021" ,
        weight:"8kg",
        amount:"35", 
        deliverspot:"Munich HBF" 
    },
    {
        img:avatar,
        name:"First card", 
        requesttext:"Request", 
        date:"3 March 2021" ,
        weight:"8kg",
        amount:"35", 
        deliverspot:"Munich HBF" 
    },
]


const TravelsList = () => {

    const history = useHistory();
const request = () => history.push('/request')


    return (
        <div className="mydeliveriesWrapper">
         
            <div className="deliveriesInner">
                {travelercard.map((items, index) => {
                  return  <TravelsCard key={index}
                                img={items.img}
                                name={items.name} 
                                requesttext={items.requesttext} 
                                date={items.date} 
                                weight={items.weight} 
                                amount={items.amount} 
                                deliverspot={items.deliverspot}
                                submitrequest={request}
                            />
                         }) }
            </div>
           
        </div>
    );
};

export default TravelsList;