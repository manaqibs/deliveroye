import React from 'react';

const TravelsCard = ({img, name,  deliverspot, weight, date, amount, requesttext, submitrequest}) => {
    return (
        <div className="deliveryCardWrapper">
            <div className="cardLeft">
                <span className="userImage"><img src={img} alt="" /></span>
                <span className="deliveryName">{name}</span>

                <span className="deliveryDate">Date: {date}</span>
                <span className="deliveryEarned">Earned: <i>${amount}</i></span>
            </div>
            <div className="cardmiddle">
                <span className="travelerLine"></span>
                <span className="travelerDestination"><i>Berlin</i> <i>Munich</i></span>
            </div>
            <div className="cardRight">

                <span className="travelerBtnWrapper">
                    <button onClick={submitrequest}>{requesttext}</button>
                </span>

            </div>
        </div>
    );
};

export default TravelsCard;