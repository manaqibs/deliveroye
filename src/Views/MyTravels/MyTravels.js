import React from 'react';
import {Header} from '../../Components/Common/index';
import TravelsList from './TravelsList';

function MyTravels(props) {
    return (
        <div className="mydeliveriesWrapper">
            <Header />           
             <TravelsList />
        </div>
    );
}

export default MyTravels;