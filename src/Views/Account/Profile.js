import React, {useState} from 'react';
import {BsPlusCircle} from 'react-icons/bs';
import {FaCheckCircle, FaRegCheckCircle, FaCamera} from 'react-icons/fa'

function Profile({name, subheading, picture, editprofile, editprefrences, phone, email, editPersonalInfo}) {

    const [verify, setVerify] = useState(false);
    const [unVerify, setUnVerify] = useState(false);

    return (
        <div className="profileWrapper">

            <span className="profileInner">
                <span className="profileHeader">
                    <span className="userName">{name} <i>{subheading}</i></span>
                    <span className="userPicture"><img src={picture} alt="" />
                    <div class="upload-btn-wrapper">
                    <button class="btn"><FaCamera /></button>
                    <input type="file" name="myfile" />
                    </div>
                    </span>
                </span>

                <span className="profileBody">
                    <ul>
                        <li onClick={editPersonalInfo}><BsPlusCircle /> {editprofile}</li>
                        <li><BsPlusCircle /> {editprefrences}</li>
                    </ul>
                </span>

                <span className="profileFooter">
                    <ul>
                        <li><FaCheckCircle />{phone}</li>
                        <li className="notVerified"><FaCheckCircle />{email}</li>
                    </ul>
                </span>
            </span>

        </div>
    );
}

export default Profile;