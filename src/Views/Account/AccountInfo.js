import React from 'react';

function AccountInfo({ratingrec, ratinggiven,notification,changepassword,payouts,paymentmathonds,privacy}) {
    return (
        <div className="accountInfoWrapper">
            <ul>
                <li>{ratingrec}</li>
                <li>{ratinggiven}</li>
                <li>{notification}</li>
                <li>{changepassword}</li>
                <li>{payouts}</li>
                <li>{paymentmathonds}</li>
                <li>{privacy}</li>
            </ul>

        </div>
    );
}

export default AccountInfo;