import React, {useState} from 'react';
import { NavLink } from 'react-router-dom';
import { Header } from '../../Components/Common';
import AccountInfo from './AccountInfo';
import Profile from './Profile';
import user from '../../resources/images/user.jpg'
import PersonalInfoModal from '../../Components/Common/Modal/PersonalInfoModal';


function Account(props) {
    const [profile, setProfile] = useState(true);
    const [account, setAccount] = useState(false);
    const [isPersonalData, setIsPersonalData] = useState(false)

    const showProfile = () => {
        setAccount(false)
        setProfile(true)
    }

    const showAccount = () => {
        setAccount(true);
        setProfile(false);
    }

    return (
        <div  className="mydeliveriesWrapper">
            <Header />

            <div className="contsctDetailWrapper">
            <div className="deliveriesInner">
             <div className="account-Tabs">
                 <span onClick={showProfile} className={`tab-nav ${profile ? 'active' : ''}`}>Profile</span>
                 <span onClick={showAccount} className={`tab-nav ${account ? 'active' : ''}`}>Account</span>
             </div>
             <div className="tabs-Body">
                 {profile &&
                    <>
                      <Profile
                         name = "Nashib ul"
                         subheading= "Neuling"
                         editprofile = "Edit profile date"
                         editprefrences = "Edit preferences"
                         phone = "+4915205928406"
                         email = "engg.nashib@gmail.com"
                         picture = {user}
                         editPersonalInfo={() => setIsPersonalData(true)}
                      />
                    </>
                 }

                 {account &&
                    <>
                        <AccountInfo
                           ratingrec= "Rating Received"
                           ratinggiven= "Rating given"
                           notification= "Notofication, Email &amp; SMS"
                           changepassword= "Change password"
                           payouts = "Payouts"
                           paymentmathonds = "Payment methods"
                           privacy = "Privacy"
                        />
                    </>
                 }

                    {isPersonalData &&
                    <PersonalInfoModal
                       handleCLose={() => setIsPersonalData(false)}
                       />
                    }

             </div>
            </div>
             </div>

        </div>
    );
}

export default Account;