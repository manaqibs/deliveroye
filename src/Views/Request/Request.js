import React from 'react';
import {AppBtn, Header, Textarea} from '../../Components/Common/index';
import {Link} from 'react-router-dom';
import { FaChevronLeft} from "react-icons/fa";


const Request = () => {
    return (
        <div className="traveleListWrapper">
            <Header />
            <div className="traveleListBody">
                <h1><i><Link to="/my-travels"><FaChevronLeft/></Link></i>Back</h1>
                <Textarea placeholder="Type you request"/>
                <AppBtn text="Send request" />
                </div>

        </div>
    );
};

export default Request;