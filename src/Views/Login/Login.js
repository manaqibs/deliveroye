import React from 'react';
import {Header, FormFields, AppBtn} from '.././../Components/Common/index';

const Login = () => {
    return (
        <div className="loginWrapper">
            <Header />
               <div className="loginInner">
                  <div className="container">
                      <h1>Sign-In</h1>
                  
                  <form>
                   <FormFields  type="text" placeholder="Enter username" />
                   <FormFields  type="password" placeholder="Enter password" />
                   <AppBtn text="Sign in" />
                  </form>
                 
                  </div> 
               </div>
        </div>
    );
};

export default Login;