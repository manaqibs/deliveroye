import React from 'react';
import { Link } from 'react-router-dom';

const Footer = () => {
    return (
        <div className="footerWrapper">
            <span>&copy; 2021 - All Rights Reserved. </span>
            <span>
                
                <ul>
                    <li><a href="#">About us</a> </li>
                    <li><a href="#">Support</a> </li>
                    <li><a href="#">FAQs</a> </li>
                </ul>
                
            </span>
        </div>
    );
};

export default Footer;