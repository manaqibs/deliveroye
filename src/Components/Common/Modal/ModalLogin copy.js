import React, {useState} from 'react';
import {Modal, FormFields} from '../index'
import { Formik } from 'formik';
import * as yup from "yup";

const validationSchema = yup.object({
    username: yup.string().email("Invalid Email").required("Email is a required field"),
    password: yup.string().required("Password is a required field").min(6, 'Your password must be longer than 6 characters.')
  })

const onFormSubmit = (e) => {   
    e.preventDefault();
}

function ModalLogin({handleCLose, handleLoginSubmit}) {

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');
    const [isLoading, setIsLoading] = useState('');

    return (
        <div>
              <Formik
                initialValues={username, password, error, isLoading}
                validationSchema={validationSchema}
                // onSubmit={(values) => {
                // setSubmittedVal("Submitted! Email: " + values.username + " Password: " + values.password)
                // }}
            >
            {({handleChange, handleBlur, handleSubmit, values, touched, errors}) => {
            return (
                <Modal 
                 closeModal  = {handleCLose}
                 submitModal = {handleLoginSubmit}
                 buttontext = "Sign In"
                 title = "Sign In"                 
                > 
                <form onSubmit={onFormSubmit}>
                   <FormFields 
                        type="text" 
                        id="username"
                        name="username" 
                        placeholder="Username" 
                        value={username}
                        onChange={(e) => setUsername({[e.target.name]: e.target.value})}
                        onBlur={handleBlur}
                        isInvalid={touched.username && errors.username}
                    />
                    <div className="input-feedback" type='invalid'>{errors.username}</div>
                            
                   <FormFields  
                    type="password" 
                    id="password"
                    name="password" 
                    value={password}
                    placeholder="Password" 
                    onChange={(e) => setPassword({[e.target.name]: e.target.value})}
                    onBlur={handleBlur}
                    isInvalid={touched.password && errors.password}
                 />                        
                    <div className="input-feedback" type='invalid'>{errors.password}</div>
                               
                  </form>                
                </Modal>
                 )}}
             </Formik>
        </div>
    );
      
}

export default ModalLogin;