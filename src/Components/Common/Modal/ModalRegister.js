import React, { useState } from "react";

import { useHistory } from "react-router-dom";
import { Modal, FormFields } from "../index";

import { useFormik } from "formik";
import * as yup from "yup";
import { connect } from "react-redux";
import { RegisterUser } from "../../../services/userServices";

function ModalRegister({ handleCLose }) {
  const history = useHistory();

  const {
    handleSubmit,
    handleChange,
    values,
    touched,
    errors,
    handleBlur,
  } = useFormik({
    initialValues: {
      name: "",
      lastname: "",
      // address: "",
      // city: "",
      // zipcode: "",
      // state: "",
      // country: "",
      useremail: "",
      username: "",
      sex: "",
      password: "",
      phonenumber: "",
    },
    validationSchema: yup.object({
      name: yup.string().required("Should not be empty."),
      lastname: yup.string().required("Should not be empty."),
      username: yup.string().required("Should not be empty."),
      sex: yup.string().required("Should not be empty."),
      // address: yup.string().required("Should not be empty."),
      // city: yup.string().required("Should not be empty."),
      // zipcode: yup.string().required("Should not be empty."),
      useremail: yup.string().required("Should not be empty."),
      password: yup.string().required("Should not be empty."),
    }),
    onSubmit: async (values) => {
      // alert(`Login: ${username}, password: ${password}`)
      const result = await registerUser(values);
      if (result) handleCLose();
      console.log(result);
    },
  });

  async function registerUser(data) {
    const result = await RegisterUser(data)
      .then((response) => {
        localStorage.setItem("user", JSON.stringify(response.data));
        return response.data;
      })
      .catch((err) => null);
    return result;
  }

  return (
    <div>
      <Modal
        closeModal={handleCLose}
        submitModal={handleSubmit}
        buttontext="Register"
        title="Register"
      >
        <form onSubmit={handleSubmit}>
          <FormFields
            type="text"
            id="name"
            name="name"
            placeholder="First name"
            value={values.name}
            onChange={handleChange}
            onBlur={handleBlur}
          />
          {touched.name && errors.name ? (
            <span className="errorMessage">{errors.name}</span>
          ) : null}
          <FormFields
            type="lastname"
            id="lastname"
            name="lastname"
            placeholder="Last name"
            value={values.lastname}
            onChange={handleChange}
            onBlur={handleBlur}
          />
          {touched.lastname && errors.lastname ? (
            <span className="errorMessage">{errors.lastname}</span>
          ) : null}
          <FormFields
            type="text"
            id="sex"
            name="sex"
            placeholder="sex"
            value={values.sex}
            onChange={handleChange}
            onBlur={handleBlur}
          />
          <FormFields
            type="text"
            id="username"
            name="username"
            placeholder="Username"
            value={values.username}
            onChange={handleChange}
            onBlur={handleBlur}
          />
          {/* <FormFields
                    type="address"
                    id="address"
                    name="address"
                    placeholder="Address"
                    value={values.address}
                    onChange={handleChange}
                    onBlur={handleBlur}
                 /> */}

          {/* <FormFields
                    type = "text"
                    name = "city"
                    placeholder = "City"
                    value = {values.city}
                    onChange = {handleChange}
                    onBlur = {handleBlur}
                /> */}

          {/* <FormFields
                    type = "text"
                    name = "zipcode"
                    placeholder = "Zip code"
                    value = {values.zipcode}
                    onChange = {handleChange}
                    onBlur = {handleBlur}
                /> */}

          <FormFields
            type="email"
            name="useremail"
            placeholder="useremail"
            value={values.useremail}
            onChange={handleChange}
            onBlur={handleBlur}
          />
          {touched.useremail && errors.useremail ? (
            <span className="errorMessage">{errors.useremail} </span>
          ) : null}

          <FormFields
            type="text"
            name="phonenumber"
            placeholder="phonenumber"
            value={values.phonenumber}
            onChange={handleChange}
            onBlur={handleBlur}
          />
          <FormFields
            type="password"
            name="password"
            placeholder="password"
            value={values.password}
            onChange={handleChange}
            onBlur={handleBlur}
          />
        </form>
      </Modal>
    </div>
  );
}

export default connect(null, null)(ModalRegister);
