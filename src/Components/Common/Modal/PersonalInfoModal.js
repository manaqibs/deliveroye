import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { Modal, FormFields, AppBtn } from "../index";


function PersonalInfoModal({ handleCLose }) {
  const [isEdit, setIsEdit] = useState(true)
   const [isSave, setIsSave] = useState(false)
  const [username, setUsername] = useState('');
  const [surname, setSurname] = useState('');
  const [gender, setGender] = useState('');
  const [yearofborth, setYearofBirth] = useState('');
  const [email, setemail] = useState('');
  const [mobile, setMobile] = useState('');
  const history = useHistory();

  const handleChange = (e) => {
    [e.target.name] = e.target.value
  }

  const handleEdit = () => {
      setIsEdit(false)
      setIsSave(true)
  }

  return (
    <div className="personalInfoWrapper">
      <Modal
        closeModal={handleCLose}
        title="Personal data"
      >
        <ul >
            <li>First name
                <i className={`${isSave ? 'editableField' : 'normalField' }`}>
                    <FormFields
                    type="text"
                    name= "username"
                    onChange={handleChange}
                    placeholder="Username"
                    />
                   <span className="editSaveBtn">
                    {isEdit && <AppBtn text="Edit" onClick={handleEdit} />}
                    {isSave && <AppBtn text="Save"/>}
                    </span>
                </i>
            </li>
            <li>Surname
                <i className={`${isSave ? 'editableField' : 'normalField' }`}>
                    <FormFields
                    type="text"
                    name= "surname"
                    onChange={handleChange}
                    placeholder="Surname"
                    />
                   <span className="editSaveBtn">
                    {isEdit && <AppBtn text="Edit" onClick={handleEdit} />}
                    {isSave && <AppBtn text="Save"/>}
                    </span>
                </i>
            </li>
            <li>Gender
                <i className={`${isSave ? 'editableField' : 'normalField' }`}>
                    <FormFields
                    type="text"
                    name= "gender"
                    onChange={handleChange}
                    placeholder="gender"
                    />
                   <span className="editSaveBtn">
                    {isEdit && <AppBtn text="Edit" onClick={handleEdit} />}
                    {isSave && <AppBtn text="Save"/>}
                    </span>
                </i>
            </li>
            <li>Year of birth
                <i className={`${isSave ? 'editableField' : 'normalField' }`}>
                    <FormFields
                    type="text"
                    name= "yeaofbirth"
                    onChange={handleChange}
                    placeholder="Year of birth"
                    />
                   <span className="editSaveBtn">
                    {isEdit && <AppBtn text="Edit" onClick={handleEdit} />}
                    {isSave && <AppBtn text="Save"/>}
                    </span>
                </i>
            </li>
            <li>E-mail address
                <i className={`${isSave ? 'editableField' : 'normalField' }`}>
                    <FormFields
                    type="text"
                    name= "email"
                    onChange={handleChange}
                    placeholder="Email"
                    />
                   <span className="editSaveBtn">
                    {isEdit && <AppBtn text="Edit" onClick={handleEdit} />}
                    {isSave && <AppBtn text="Save"/>}
                    </span>
                </i>
            </li>
            <li>Mobile number
                <i className={`${isSave ? 'editableField' : 'normalField' }`}>
                    <FormFields
                    type="text"
                    name= "phonenumber"
                    onChange={handleChange}
                    placeholder="Phone number"
                    />
                   <span className="editSaveBtn">
                    {isEdit && <AppBtn text="Edit" onClick={handleEdit} />}
                    {isSave && <AppBtn text="Save"/>}
                    </span>
                </i>
            </li>
        </ul>

      </Modal>
    </div>
  );
}

export default PersonalInfoModal;
