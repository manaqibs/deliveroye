import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { Modal, FormFields } from "../index";

import { useFormik } from "formik";
import * as yup from "yup";
import { getUser } from "../../../services/userServices";

function ModalLogin({ handleCLose }) {
  const history = useHistory();

  useEffect(() => {
    if (localStorage.getItem("user-info")) {
      history.push("/traveller-list");
    }
  }, []);

  const {
    handleSubmit,
    handleChange,
    values,
    touched,
    errors,
    handleBlur,
  } = useFormik({
    initialValues: {
      useremail: "",
      password: "",
    },
    validationSchema: yup.object({
      useremail: yup.string().required("Should not be empty."),
      password: yup.string().required("Should not be empty."),
    }),
    onSubmit: async (values) => {
      alert(`Login: ${values.useremail}, password: ${values.password}`);
      const result = await login(values);
      if (result) history.push("/traveller-list");
    },
  });

  async function login(data) {
    const result = await getUser(data)
      .then((response) => {
        localStorage.setItem("user", JSON.stringify(response.data));
        return response.data;
      })
      .catch((err) => null);
    return result;
  }

  return (
    <div>
      <Modal
        closeModal={handleCLose}
        submitModal={handleSubmit}
        buttontext="Sign In"
        title="Sign In"
      >
        <form onSubmit={handleSubmit}>
          <FormFields
            type="text"
            id="useremail"
            name="useremail"
            placeholder="useremail"
            value={values.useremail}
            onChange={handleChange}
            onBlur={handleBlur}
          />
          {touched.useremail && errors.useremail ? (
            <span className="errorMessage">{errors.useremail}</span>
          ) : null}
          <FormFields
            type="password"
            id="password"
            name="password"
            placeholder="Password"
            value={values.password}
            onChange={handleChange}
            onBlur={handleBlur}
          />
          {touched.password && errors.password ? (
            <span className="errorMessage">{errors.password}</span>
          ) : null}
        </form>
      </Modal>
    </div>
  );
}

export default ModalLogin;
