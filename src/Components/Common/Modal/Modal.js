import React from 'react';
import {BsX} from 'react-icons/bs'

function Modal({closeModal, submitModal, children, title,  buttontext, type}) {
    return (        
        <div className="overlay">
            <div className="popupWrapper">
                <span onClick={closeModal} className="popupClose"><BsX /></span>
                <h1>{title}</h1>
                <div className="popupInnerBody">
                    {children}
                    <div className="popupFooter">
                    <button type={type} onClick={submitModal}>{buttontext}</button>
                </div> 
                </div>
                
            </div>
        </div>
      
    );
}

export default Modal;