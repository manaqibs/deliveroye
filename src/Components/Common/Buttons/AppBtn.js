import React from 'react';

const AppBtn = ({text, onClick}) => {
    return (
        <div className="btnWrapper">
            <button onClick={onClick}>{text}</button>
        </div>
    );
};

export default AppBtn;