import React from "react";

const FormFields = ({
  value,
  placeholder,
  type,
  children,
  name,
  onChange,
  onFocus,
  onBlur,
  min,
}) => {
  return (
    <div className="formFieldsWrapper">
      <input
        value={value}
        placeholder={placeholder}
        type={type}
        name={name}
        onChange={onChange}
        autoComplete="off"
        onFocus={onFocus}
        onBlur={onBlur}
        min={min}
      />
      <span className="errorMessage">{children}</span>
    </div>
  );
};

export default FormFields;
