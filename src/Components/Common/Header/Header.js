import React, {useState} from 'react';
import {Link, NavLink} from 'react-router-dom';
import { FaSmileBeam, FaBars } from "react-icons/fa";
import { useHistory } from "react-router-dom";
import {ModalLogin} from '../index'
import ModalRegister from '../Modal/ModalRegister';
import {connect} from 'react-redux';



const Header = (props) => {


const showRegNav = () => (
    setShowRgister(true),
    setIsShowNav(false)
)

const showLoginNav = () => (
    setShowHeaderLogin(true),
    setIsShowNav(false)
)

    console.log(props.username)
    const [showHeaderLogin, setShowHeaderLogin] = useState(false)
    const [showRegister, setShowRgister] = useState(false)
    const [isShowNav, setIsShowNav] = useState(false);

    const history = useHistory();
    const travelist = () => history.push('/traveller-list');
    return (
        <>
        <div className="headerWrapper">
            <span className="siteLogo"><Link to="/">DeliverOye <FaSmileBeam /></Link></span>
            <span className="MobileBarNav" onClick={()=> setIsShowNav(!isShowNav)}><FaBars /></span>
            <span className={`${isShowNav ? 'mobileNavOpen' : 'mobileNav'}`}><ul>
                <li onClick={showRegNav}>Sign-up</li>
                <li onClick={showLoginNav}>Login</li>
                <li><NavLink to="/my-travels">My travels</NavLink></li>
                <li><NavLink to="/my-deliveries">My deliveries</NavLink></li>
                <li><NavLink to="/account">Account</NavLink></li>
                <li>Support</li>
                <li>Logout</li>
                {/* <li onClick={() => setShowHeaderLogin(true)}>Login</li> */}
                </ul>
             </span>
        </div>
          {/* Login popup */}
          {showHeaderLogin && <ModalLogin
                handleCLose = {() => setShowHeaderLogin(false)}
                handleLoginSubmit = {travelist}
           />}

           {/* Register popup */}
           {showRegister && <ModalRegister
                handleCLose = {() => setShowRgister(false)}
                handleRegisterSubmit = {() => alert('ok')}
            />}
        </>
    );
};

const mapStatetoProps = (state) => {
    return state
}


export default connect(mapStatetoProps, null)(Header);