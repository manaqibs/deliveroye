// const baseURL = "https://deliveroye-api.herokuapp.com/api/"
const baseURL = "http://localhost:3000/";

export const apiURL = "http://localhost:3000";

const config = {
  REST_API: {
    Package: {
      GetPackage: `${baseURL}packages`,
    },
    User: {
      GetUser: `${baseURL}user`,
      RegisterUser: `${baseURL}user`,
    },
  },
};

export const UsersAPIs = {
  GetUser: `${baseURL}api/user`,
  RegisterUser: `${baseURL}api/user`,
};

export default config;
