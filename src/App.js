
import React from 'react';
import './App.css';
import {Routes} from '../src/routes';
import { Footer } from './Components/Common';

function App() {
  return <> <Routes /> <span><Footer /></span></>
}

export default App;
