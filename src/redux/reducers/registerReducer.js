import * as types from '../actions/actionTypes';

const INITIAL_STATE = {
     registerDetail: {}
}

export default (state, action) => {
  if(!state) state = INITIAL_STATE;

  switch(action.type){
    case types.USER_REGISTER.SET_USER_REGISTER:
        return {
                 ...state,
                 registerDetail: action.payload
        }
     default:
         return state
  }
}