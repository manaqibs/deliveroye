import * as types from '../actions/actionTypes';

const INITIAL_STATE = {
      userDetail: {}
}

export default (state, action) => {
    if(!state) state = INITIAL_STATE;

    switch(action.type) {
        case types.USER_ACTIONS.SET_USER_DETAILS:
        {
            return {
                ...state,
                userDetail: action.payload
            }

        }
        default:
            return state;
    }
}