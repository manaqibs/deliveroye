import {combineReducers} from 'redux'
import appReducer from './appReducer';
import registerReducer from './registerReducer';
import userReducer from './userReducer'

export default combineReducers({
    app: appReducer,
    userdetail: userReducer,
    registerdetail: registerReducer
});