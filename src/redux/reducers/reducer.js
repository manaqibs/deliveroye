const INITIAL_STATE = {
    username: '',
    password: '',
    isLoading: false   
  };
const userReducer = (state = INITIAL_STATE, action) => {
    if (!state) state = INITIAL_STATE;
    switch (action.type) {
      case 'LOGIN':      
        return { state};
      
      default:
        return state;
    }
  };

export default userReducer;